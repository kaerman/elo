# MyElo Console Application

A ranking program using the Elo ranking algorithm.

### Prerequisites

Java JDK 11 or above and Maven

### Installing
Linux\
export JAVA_HOME="/opt/java/jdk-11.0.9"

Compile -> ./mvnw compile\
Package -> ./mvnw package\
Install -> ./mvnw install

### Running the tests

Test   ->  ./mvnw test

### Running MyElo
Run in target directory -> java -jar MyElo.jar ../idNameFile.txt ../idScoreFile.txt

Commands -> [competitors, match_suggest, show_rank_all, competitor_history (competitorId)], in order to exit type [quit]

Command = [competitors] -> print all competitors\
Command = [match_suggest] -> suggest match according to similar scores\
Command = [show_rank_all] -> print all competitors order by rank descending\
Command = [competitor_history (competitorId)] -> print competitor history according to given competitor id.\
Command = [quit] -> exit program. 

