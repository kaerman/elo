package org.kaerman.myelo.util;

public class EloUtil {
    // All constants in below link
    //https://en.wikipedia.org/wiki/Elo_rating_system#Most_accurate_K-factor

    public static final int DEFAULT_RANK = 2200;

    //The USCF (which makes use of a logistic distribution as opposed to a normal distribution) formerly staggered the K-factor according to three main rating ranges of:
    //
    //Players below 2100: K-factor of 32 used
    //Players between 2100 and 2400: K-factor of 24 used
    //Players above 2400: K-factor of 16 used.
    public static int calculateKFactor(int score) {
        if (score > 2400)
            return 16;
        if (score < 2100)
            return 32;
        return 24;
    }

    public static double calculateWinProbability (int winnerScore, int loserScore) {
        return (1.0 / (1.0 + Math.pow(10, ((double)(loserScore-winnerScore) / 400))));
    }

    public static int calculateEloRating(int score, double probability, boolean isWinner) {
        double newScore = score + (calculateKFactor(score)*((isWinner ? 1 : 0 ) - probability));
        return (int) (isWinner ? Math.floor(newScore) : Math.ceil(newScore));
    }

}
