package org.kaerman.myelo.util;

import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.domain.CompetitorMatchHistory;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static org.kaerman.myelo.domain.Competitor.*;
import static org.kaerman.myelo.domain.MatchResult.*;

public class PrintUtil {

    private final PrintStream printStream;

    public PrintUtil(PrintStream printStream) {
        this.printStream = printStream;
    }

    public void println(String line) {
        printStream.println(line);
    }

    public void printCompetitorRankList(List<Competitor> competitorOrderedList) {
        IntStream.range(0, competitorOrderedList.size()).forEach(index -> {
            Competitor competitor = competitorOrderedList.get(index);
            printStream.println("Order " + (index+1) +
                    ", rank point " + competitor.getRank() + ", competitor id " + competitor.getId().getValue() +
                    ", win " + competitor.getNumberOfWin() + ", lost " + competitor.getNumberOfLost());
        });
    }

    public void printCompetitorMap(Map<CompetitorId, Competitor> competitorMap) {
        competitorMap.forEach((integer, competitor) -> printStream.println(competitor));
    }

    public void printCompetitorHistory(Map<MatchId, CompetitorMatchHistory.MatchResultHistory> competitorMatchResultMap) {
        competitorMatchResultMap.forEach((key, value) -> printStream.println(value));
    }

    public void printMatchSuggests(Map<Competitor, Competitor> suggestedMatches) {
        suggestedMatches.forEach((competitorId, opponentId) -> printStream.println("Match " + competitorId + ", " + opponentId));
    }

    public void printSectionExtract() {
        printStream.println("-----------------------------------------------------------------------");
    }
}
