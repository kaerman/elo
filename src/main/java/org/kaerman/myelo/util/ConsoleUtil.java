package org.kaerman.myelo.util;

import org.kaerman.myelo.event.Event;
import org.kaerman.myelo.event.ReportEvent;
import org.kaerman.myelo.parser.Parser;

import java.util.Scanner;

import static org.kaerman.myelo.event.ReportEvent.*;

public class ConsoleUtil {

    private static final String CompetitorCommand = "competitors";
    private static final String MatchSuggest = "match_suggest";
    private static final String ShowRankAll = "show_rank_all";
    private static final String CompetitorHistory = "competitor_history";
    private static final String Quit = "quit";


    private static final Scanner scanner = new Scanner(System.in);
    private static final PrintUtil print = new PrintUtil(System.out);
    public static final int COMPETITOR_HISTORY_NUMBER_OF_PARAMETER = 2;

    public static void printMenu() {
        print.println("Commands -> [competitors, match_suggest, show_rank_all, competitor_history (competitorId)], in order to exit type [quit]");
    }

    public static Event<?> readCommand() {
        String line = scanner.nextLine().trim().toLowerCase();
        switch (line) {
            case CompetitorCommand:
                return new ReportEvent(ReportTypeAll.COMPETITOR_LIST);
            case MatchSuggest:
                return new ReportEvent(ReportTypeAll.COMPETITOR_SUGGEST);
            case ShowRankAll:
                return new ReportEvent(ReportTypeAll.COMPETITOR_RANK);
            case Quit:
                System.exit(0);
            default:
                if (line.startsWith(CompetitorHistory)) {
                    String[] params = line.split(Parser.WHITE_SPACE);
                    if (params.length == COMPETITOR_HISTORY_NUMBER_OF_PARAMETER) {
                        try {
                            return new ReportEvent(Integer.parseInt(params[1]), ReportTypeCompetitor.COMPETITOR_HIST);
                        } catch (NumberFormatException ignored) {}
                    }
                }
                return new ReportEvent(ReportTypeAll.MENU);
        }
    }

}
