package org.kaerman.myelo.consumer;

import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.domain.CompetitorBoard;
import org.kaerman.myelo.domain.MatchResult;
import org.kaerman.myelo.event.Event;
import org.kaerman.myelo.event.MatchEvent;

import java.util.*;

import static org.kaerman.myelo.event.Event.*;

public class HistoryConsumer implements Consumer {
    @Override
    public List<EventType> registerEvents() {
        return Collections.singletonList(EventType.MATCH_EVENT);
    }

    @Override
    public void consume(Event<?> event) {
        Objects.requireNonNull(event);
        if (EventType.MATCH_EVENT.equals(event.getEventType())) {
            MatchEvent matchEvent = (MatchEvent) event;
            addHistory(matchEvent.getEvent());
        }
    }

    private void addHistory(MatchResult matchResult) {
        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
        Map<Competitor.CompetitorId, Competitor> competitorMap = competitorBoard.getCompetitorMap();
        Competitor winnerCompetitor = competitorMap.get(matchResult.getWinnerCompetitorId());
        Competitor loserCompetitor = competitorMap.get(matchResult.getLoserCompetitorId());

        if (Objects.nonNull(winnerCompetitor) && Objects.nonNull(loserCompetitor)) {
            competitorBoard.addMatchResultMap(matchResult);
            addCompetitorHistory(matchResult.getWinnerCompetitorId(), matchResult.getMatchId());
            addCompetitorHistory(matchResult.getLoserCompetitorId(), matchResult.getMatchId());
            removeAvailableOpponentSet(matchResult.getWinnerCompetitorId(), matchResult.getLoserCompetitorId());
            removeAvailableOpponentSet(matchResult.getLoserCompetitorId(), matchResult.getWinnerCompetitorId());
        } else {
            throw new IllegalArgumentException("Match competitors are not exits " + matchResult);
        }
    }

    private void addCompetitorHistory(Competitor.CompetitorId competitorId, MatchResult.MatchId matchId) {
        CompetitorBoard.getInstance().getCompetitorHistory(competitorId).addMatch(matchId);
    }


    private void removeAvailableOpponentSet(Competitor.CompetitorId competitorId, Competitor.CompetitorId opponentCompetitorId) {
        CompetitorBoard.getInstance().getCompetitorsAvailableOpponents(competitorId).removeOpponent(opponentCompetitorId);
    }
}
