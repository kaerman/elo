package org.kaerman.myelo.consumer;

import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.domain.CompetitorBoard;
import org.kaerman.myelo.event.Event;

import java.util.*;

import static org.kaerman.myelo.domain.Competitor.*;
import static org.kaerman.myelo.event.Event.*;

public class MatchSuggestConsumer implements Consumer{

    public static final int MAX_OPPONENT_RANGE_DIFFERENCE = 25;
    private final Random random = new Random();

    @Override
    public List<EventType> registerEvents() {
        return Collections.singletonList(EventType.SUGGEST_EVENT);
    }

    @Override
    public void consume(Event<?> event) {
        Objects.requireNonNull(event);
        if (EventType.SUGGEST_EVENT.equals(event.getEventType())) {
            CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
            Map<CompetitorId, CompetitorId> suggestMatchMap = new HashMap<>();
            competitorBoard.getCompetitorMap().forEach((competitorId, competitor) -> {
                if (suggestMatchMap.containsKey(competitorId))
                    return;
                boolean isMatchSuggested = matchSuggestAccordingToRange(competitor, suggestMatchMap);
                if (!isMatchSuggested) {
                    matchSuggestAccordingToRandomIndex(competitorBoard, suggestMatchMap, competitorId, competitor);
                }
            });
            competitorBoard.setMatchSuggest(suggestMatchMap);
        }
    }

    private boolean matchSuggestAccordingToRange(Competitor competitor, Map<CompetitorId, CompetitorId> suggestMatchMap) {
        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
        Set<CompetitorId> opponents = competitorBoard.getCompetitorsAvailableOpponents(competitor.getId()).getOpponentCompetitorSet();

        boolean isMatchSuggested = false;
        for (CompetitorId opponentId : opponents) {
            if (suggestMatchMap.containsKey(opponentId))
                continue;
            Competitor opponent = competitorBoard.getCompetitor(opponentId);
            if (Math.abs(competitor.getRank() - opponent.getRank()) < MAX_OPPONENT_RANGE_DIFFERENCE) {
                addSuggestMatchMap(suggestMatchMap, opponentId, competitor.getId());
                isMatchSuggested = true;
                break;
            }
        }
        return isMatchSuggested;
    }

    private void matchSuggestAccordingToRandomIndex(CompetitorBoard competitorBoard, Map<CompetitorId, CompetitorId> suggestMatchMap, CompetitorId competitorId, Competitor competitor) {
        Set<CompetitorId> opponents = competitorBoard.getCompetitorsAvailableOpponents(competitor.getId()).getOpponentCompetitorSet();
        int index = Math.abs(random.nextInt(opponents.size()));
        Iterator<CompetitorId> iterator = opponents.iterator();
        CompetitorId opponentId = null;
        for (int i = 0; i <= index; i++) {
            opponentId = iterator.next();
        }
        addSuggestMatchMap(suggestMatchMap, competitorId, opponentId);
    }

    private void addSuggestMatchMap(Map<CompetitorId, CompetitorId> suggestedMatch, CompetitorId competitorId, CompetitorId opponentId) {
        suggestedMatch.put(opponentId, competitorId);
        suggestedMatch.put(competitorId, opponentId);
    }
}
