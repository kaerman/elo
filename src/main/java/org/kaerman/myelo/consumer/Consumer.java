package org.kaerman.myelo.consumer;

import org.kaerman.myelo.event.Event;

import java.util.List;

import static org.kaerman.myelo.event.Event.*;

public interface Consumer {
    List<EventType> registerEvents();
    void consume(Event<?> event);
}
