package org.kaerman.myelo.consumer;

import org.kaerman.myelo.event.Event;

import java.util.*;

import static org.kaerman.myelo.event.Event.*;

public class ConsumerRegistry {
    private static final ConsumerRegistry singletonInstance = new ConsumerRegistry();
    private final HashMap<EventType, Set<Consumer>> consumerEventMap;

    public static ConsumerRegistry getInstance() {
        return singletonInstance;
    }

    private ConsumerRegistry() {
        this.consumerEventMap = new HashMap<>();
        Arrays.stream(EventType.values())
                .forEach(eventType -> consumerEventMap.put(eventType, new HashSet<>()));
    }

    public void addConsumer(Consumer consumer) {
        Objects.requireNonNull(consumer);
        consumer.registerEvents().forEach(
                eventType -> {
                    Set<Consumer> consumerSet = consumerEventMap.get(eventType);
                    consumerSet.add(consumer);
                }
        );
    }

    public void fireEvent(Event<?> event) {
        Objects.requireNonNull(event);
        Set<Consumer> consumerSet = consumerEventMap.get(event.getEventType());
        consumerSet.forEach(consumer -> trigger(consumer, event));
    }

    private void trigger(Consumer consumer, Event<?> event) {
        try {
            consumer.consume(event);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
