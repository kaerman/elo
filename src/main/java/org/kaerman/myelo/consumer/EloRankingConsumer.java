package org.kaerman.myelo.consumer;

import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.domain.CompetitorBoard;
import org.kaerman.myelo.domain.MatchResult;
import org.kaerman.myelo.event.Event;
import org.kaerman.myelo.event.Event.EventType;
import org.kaerman.myelo.event.MatchEvent;
import org.kaerman.myelo.util.EloUtil;

import java.util.*;

import static org.kaerman.myelo.domain.Competitor.*;

public class EloRankingConsumer implements Consumer{
    @Override
    public List<EventType> registerEvents() {
        return Collections.singletonList(EventType.MATCH_EVENT);
    }

    @Override
    public void consume(Event<?> event) {
        Objects.requireNonNull(event);
        if (EventType.MATCH_EVENT.equals(event.getEventType())) {
            MatchEvent matchEvent = (MatchEvent) event;
            MatchResult matchResult = matchEvent.getEvent();
            calculateNewRating(matchResult);
        }
    }

    private void calculateNewRating(MatchResult matchResult) {
        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
        Map<CompetitorId, Competitor> competitorMap = competitorBoard.getCompetitorMap();
        Competitor winnerCompetitor = competitorMap.get(matchResult.getWinnerCompetitorId());
        Competitor loserCompetitor = competitorMap.get(matchResult.getLoserCompetitorId());

        if (Objects.nonNull(winnerCompetitor) && Objects.nonNull(loserCompetitor)) {
            winnerCompetitor.incrementWin();
            double winnerProbability = EloUtil.calculateWinProbability(winnerCompetitor.getRank(), loserCompetitor.getRank());
            winnerCompetitor.setRank(EloUtil.calculateEloRating(winnerCompetitor.getRank(), winnerProbability, true));

            loserCompetitor.incrementLost();
            double loserProbability = 1 - winnerProbability;
            loserCompetitor.setRank(EloUtil.calculateEloRating(loserCompetitor.getRank(), loserProbability, false));
        } else {
            throw new IllegalArgumentException("Match competitors are not exits " + matchResult);
        }
    }
}
