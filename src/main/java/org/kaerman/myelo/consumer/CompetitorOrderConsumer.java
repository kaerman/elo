package org.kaerman.myelo.consumer;

import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.domain.CompetitorBoard;
import org.kaerman.myelo.event.Event;

import java.util.*;
import java.util.stream.Collectors;

import static org.kaerman.myelo.domain.Competitor.*;
import static org.kaerman.myelo.event.Event.*;

public class CompetitorOrderConsumer implements Consumer{
    @Override
    public List<EventType> registerEvents() {
        return Collections.singletonList(EventType.ORDER_EVENT);
    }

    @Override
    public void consume(Event<?> event) {
        Objects.requireNonNull(event);
        if (EventType.ORDER_EVENT.equals(event.getEventType())) {
            CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
            Map<CompetitorId, Competitor> competitorMap = competitorBoard.getCompetitorMap();
            List<Competitor> competitorList = competitorMap.entrySet().stream()
                    .sorted(Collections.reverseOrder(Comparator.comparingInt(competitor -> competitor.getValue().getRank())))
                    .map(Map.Entry::getValue)
                    .collect(Collectors.toList());
            competitorBoard.setCompetitorRankList(competitorList);
        }
    }
}
