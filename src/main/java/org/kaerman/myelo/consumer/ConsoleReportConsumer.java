package org.kaerman.myelo.consumer;

import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.domain.CompetitorBoard;
import org.kaerman.myelo.event.Event;
import org.kaerman.myelo.event.ReportEvent.ReportTypeAll;
import org.kaerman.myelo.util.PrintUtil;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.kaerman.myelo.domain.Competitor.*;
import static org.kaerman.myelo.event.Event.*;
import static org.kaerman.myelo.event.ReportEvent.*;

public class ConsoleReportConsumer implements Consumer {

    private final PrintUtil print;

    public ConsoleReportConsumer() {
        print = new PrintUtil(System.out);
    }

    @Override
    public List<EventType> registerEvents() {
        return Collections.singletonList(EventType.REPORT_EVENT);
    }

    @Override
    public void consume(Event<?> event) {
        Objects.requireNonNull(event);
        if (EventType.REPORT_EVENT.equals(event.getEventType())) {
            Report report = (Report) event.getEvent();
            if (report.getType() instanceof ReportTypeAll) {
                switch ((ReportTypeAll)report.getType()) {
                    case COMPETITOR_LIST:
                        printCompetitorList();
                        break;
                    case COMPETITOR_RANK:
                        printCompetitorRankList();
                        break;
                    case COMPETITOR_SUGGEST:
                        printMatchSuggests();
                        break;
                    case ALL:
                        printCompetitorList();
                        printCompetitorRankList();
                        break;
                    default:
                }
            }
            if (report.getType() instanceof ReportTypeCompetitor) {
                printCompetitorHistory(report.getCompetitorId());
            }
        }
    }

    private void printCompetitorList() {
        print.println("Competitor List");
        print.printCompetitorMap(CompetitorBoard.getInstance().getCompetitorMap());
        print.printSectionExtract();
    }

    private void printCompetitorRankList() {
        print.println("Competitor Rank List");
        print.printCompetitorRankList(CompetitorBoard.getInstance().getCompetitorRankList());
        print.printSectionExtract();
    }

    private void printMatchSuggests() {
        print.println("Match Suggest List");
        print.printMatchSuggests(CompetitorBoard.getInstance().getSuggestedMatches());
        print.printSectionExtract();
    }

    private void printCompetitorHistory(CompetitorId competitorId) {
        print.println("Competitor " + competitorId + " History");
        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();

        Competitor competitor = competitorBoard.getCompetitor(competitorId);
        if (Objects.nonNull(competitor)) {
            print.println(competitor.toString());
            print.printCompetitorHistory(competitorBoard.getCompetitorHistory(competitorId).getCompetitorMatchResultList());
        } else {
            print.println("Competitor Id " + competitorId + " not exists");
        }
        print.printSectionExtract();
    }
}
