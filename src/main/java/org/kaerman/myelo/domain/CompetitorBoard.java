package org.kaerman.myelo.domain;

import org.kaerman.myelo.consumer.*;

import java.util.*;

import static org.kaerman.myelo.domain.Competitor.*;
import static org.kaerman.myelo.domain.MatchResult.*;

public class CompetitorBoard {

    private static final CompetitorBoard singletonInstance = new CompetitorBoard();

    private List<Competitor> competitorRankList;
    private Map<Competitor, Competitor> suggestedMatches;
    private final HashMap<CompetitorId, Competitor> competitorMap;
    private final HashMap<MatchId, MatchResult> matchResultMap;
    private final HashMap<CompetitorId, CompetitorMatchHistory> competitorsMatchHistory;
    private final HashMap<CompetitorId, CompetitorAvailableOpponents> competitorsAvailableOpponents;


    public static CompetitorBoard getInstance() {
        return singletonInstance;
    }

    private CompetitorBoard() {
        competitorRankList = new ArrayList<>();
        competitorMap = new HashMap<>();
        matchResultMap = new HashMap<>();
        competitorsMatchHistory = new HashMap<>();
        competitorsAvailableOpponents = new HashMap<>();

        EloRankingConsumer eloRankingConsumer = new EloRankingConsumer();
        HistoryConsumer historyConsumer = new HistoryConsumer();
        CompetitorOrderConsumer competitorOrderConsumer = new CompetitorOrderConsumer();
        MatchSuggestConsumer matchSuggestConsumer = new MatchSuggestConsumer();
        ConsoleReportConsumer consoleReportConsumer = new ConsoleReportConsumer();

        ConsumerRegistry consumerRegistry = ConsumerRegistry.getInstance();
        consumerRegistry.addConsumer(eloRankingConsumer);
        consumerRegistry.addConsumer(historyConsumer);
        consumerRegistry.addConsumer(competitorOrderConsumer);
        consumerRegistry.addConsumer(matchSuggestConsumer);
        consumerRegistry.addConsumer(consoleReportConsumer);
    }

    public void loadCompetitors(Map<CompetitorId, Competitor> competitorMap) {
        this.competitorMap.putAll(competitorMap);
        competitorMap.forEach((competitorId, competitor) -> {
            competitorsMatchHistory.put(competitorId, new CompetitorMatchHistory(competitorId));
            Set<CompetitorId> competitorIds = new HashSet<>(competitorMap.keySet());
            competitorIds.remove(competitorId);
            competitorsAvailableOpponents.put(competitorId, new CompetitorAvailableOpponents(competitorIds));
        });
    }

    public void addMatchResultMap(MatchResult matchResult) {
        matchResultMap.put(matchResult.getMatchId(), matchResult);
    }

    public  Map<CompetitorId, Competitor> getCompetitorMap() {
        return  Collections.unmodifiableMap(competitorMap);
    }

    public Map<MatchId, MatchResult> getMatchResultMap() {
        return Collections.unmodifiableMap(matchResultMap);
    }

    public List<Competitor> getCompetitorRankList() {
        return Collections.unmodifiableList(competitorRankList);
    }

    public void setCompetitorRankList(List<Competitor> competitorRankList) {
        this.competitorRankList = competitorRankList;
    }

    public CompetitorMatchHistory getCompetitorHistory(CompetitorId competitorId) {
        return competitorsMatchHistory.get(competitorId);
    }

    public Competitor getCompetitor(CompetitorId competitorId) {
        return competitorMap.get(competitorId);
    }

    public MatchResult getMatchResult(MatchId matchId) {
        return matchResultMap.get(matchId);
    }


    public CompetitorAvailableOpponents getCompetitorsAvailableOpponents(CompetitorId competitorId) {
        return competitorsAvailableOpponents.get(competitorId);
    }

    public void setMatchSuggest(Map<CompetitorId, CompetitorId> suggestedMatches) {
        this.suggestedMatches = new HashMap<>();
        suggestedMatches.forEach((competitorId, opponentId) -> {
            if (!this.suggestedMatches.containsKey(competitorMap.get(competitorId))) {
                this.suggestedMatches.put(competitorMap.get(opponentId), competitorMap.get(competitorId));
            }
        });
    }

    public Map<Competitor, Competitor> getSuggestedMatches() {
        return Collections.unmodifiableMap(suggestedMatches);
    }
}
