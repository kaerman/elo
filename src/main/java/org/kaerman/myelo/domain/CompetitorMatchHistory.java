package org.kaerman.myelo.domain;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.kaerman.myelo.domain.Competitor.*;
import static org.kaerman.myelo.domain.MatchResult.*;

public class CompetitorMatchHistory {

    private final CompetitorId competitorId;
    private final LinkedHashMap<MatchId, MatchResultHistory> competitorMatchHistoryMap;

    public static class MatchResultHistory {
        Competitor competitor;
        MatchResult matchResult;

        public MatchResultHistory(Competitor competitor, MatchResult matchResult) {
            this.competitor = new Competitor(competitor);
            this.matchResult = new MatchResult(matchResult);
        }

        @Override
        public String toString() {
            return "MatchResultHistory{" +
                    competitor +
                    ", " +
                    matchResult +
                    '}';
        }

        public Competitor getCompetitor() {
            return competitor;
        }

        public MatchResult getMatchResult() {
            return matchResult;
        }
    }

    public CompetitorMatchHistory(CompetitorId competitorId) {
        this.competitorId = competitorId;
        competitorMatchHistoryMap = new LinkedHashMap<>();
    }

    public Map<MatchId, MatchResultHistory> getCompetitorMatchResultList() {
        return competitorMatchHistoryMap;
    }

    public void addMatch(MatchId matchId) {
        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
        Competitor competitor = competitorBoard.getCompetitor(competitorId);
        MatchResult matchResult = competitorBoard.getMatchResult(matchId);
        competitorMatchHistoryMap.put(matchId, new MatchResultHistory(competitor, matchResult));
    }



}
