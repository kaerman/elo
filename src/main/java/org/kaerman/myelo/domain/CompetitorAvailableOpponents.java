package org.kaerman.myelo.domain;

import java.util.Set;

import static org.kaerman.myelo.domain.Competitor.*;

public class CompetitorAvailableOpponents {

    private final Set<CompetitorId> opponentCompetitorSet;
    
    public CompetitorAvailableOpponents(Set<CompetitorId> allOpponents) {
        this.opponentCompetitorSet = allOpponents;
    }

    public void removeOpponent(CompetitorId opponentCompetitorId) {
        opponentCompetitorSet.remove(opponentCompetitorId);
    }

    public Set<CompetitorId> getOpponentCompetitorSet() {
        return opponentCompetitorSet;
    }
}
