package org.kaerman.myelo.domain;

import static org.kaerman.myelo.domain.Competitor.*;

public class MatchResult {

    private final MatchId matchId;
    private final CompetitorId winnerCompetitorId;
    private final CompetitorId loserCompetitorId;

    public static class MatchId {
        final int value;

        public static MatchId of(int matchId) {
            return new MatchId(matchId);
        }

        private MatchId(int id) {
            this.value = id;
        }

        public int getValue() {
            return value;
        }

        @Override
        public int hashCode() {
            return value;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof MatchId) {
                return value == ((MatchId)obj).getValue();
            }
            return false;
        }

        @Override
        public String toString() {
            return "MatchId{" +
                    "value=" + value +
                    '}';
        }
    }

    public MatchResult(int matchId, CompetitorId winnerCompetitorId, CompetitorId loserCompetitorId) {
        this.matchId = MatchId.of(matchId);
        this.winnerCompetitorId = winnerCompetitorId;
        this.loserCompetitorId = loserCompetitorId;
    }

    public MatchResult(MatchResult matchResult) {
        this.matchId = matchResult.matchId;
        this.winnerCompetitorId = matchResult.getWinnerCompetitorId();
        this.loserCompetitorId = matchResult.getLoserCompetitorId();
    }

    public MatchResult(int winnerCompetitorId, int loserCompetitorId) {
        this.matchId = MatchId.of(0);
        this.winnerCompetitorId = CompetitorId.of(winnerCompetitorId);
        this.loserCompetitorId = CompetitorId.of(loserCompetitorId);
    }

    public CompetitorId getWinnerCompetitorId() {
        return winnerCompetitorId;
    }

    public CompetitorId getLoserCompetitorId() {
        return loserCompetitorId;
    }

    public MatchId getMatchId() {
        return matchId;
    }

    @Override
    public String toString() {
        return "MatchResult{" +
                "matchId=" + matchId.value +
                ", winnerCompetitorId=" + winnerCompetitorId.value +
                ", loserCompetitorId=" + loserCompetitorId.value +
                '}';
    }
}
