package org.kaerman.myelo.domain;

import org.kaerman.myelo.util.EloUtil;

import java.util.Objects;

public class Competitor {

    private final CompetitorId competitorId;
    private final String name;

    private int rank;
    private int numberOfWin;
    private int numberOfLost;

    public static class CompetitorId {
        final int value;

        public static CompetitorId of(int competitorId) {
            return new CompetitorId(competitorId);
        }

        private CompetitorId(int id) {
            this.value = id;
        }

        public int getValue() {
            return value;
        }

        @Override
        public int hashCode() {
            return value;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof CompetitorId) {
                return value == ((CompetitorId)obj).getValue();
            }
            return false;
        }

        @Override
        public String toString() {
            return "CompetitorId{" +
                    "value=" + value +
                    '}';
        }
    }

    public Competitor(int id, String name) {
        this.competitorId = new CompetitorId(id);
        this.name = name;
        rank = EloUtil.DEFAULT_RANK;
        numberOfWin = 0;
        numberOfLost = 0;
    }


    public Competitor(Competitor competitor) {
        this.competitorId = competitor.competitorId;
        this.name = competitor.name;
        rank = competitor.rank;
        numberOfWin = competitor.numberOfWin;
        numberOfLost = competitor.numberOfLost;
    }


    public int getNumberOfWin() {
        return numberOfWin;
    }

    public int incrementWin() {
        numberOfWin++;
        return numberOfWin;
    }

    public int getNumberOfLost() {
        return numberOfLost;
    }

    public int incrementLost() {
        numberOfLost++;
        return numberOfLost;
    }

    public Competitor.CompetitorId getId() {
        return competitorId;
    }

    public String getName() {
        return name;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Competitor that = (Competitor) o;
        return rank == that.rank && numberOfWin == that.numberOfWin && numberOfLost == that.numberOfLost && competitorId.equals(that.competitorId) && name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(competitorId, name, rank, numberOfWin, numberOfLost);
    }

    @Override
    public String toString() {
        return "Competitor{" +
                "id=" + competitorId.value +
                ", name='" + name + '\'' +
                ", rank=" + rank +
                '}';
    }
}
