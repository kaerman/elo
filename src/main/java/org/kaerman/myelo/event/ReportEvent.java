package org.kaerman.myelo.event;

import org.kaerman.myelo.domain.Competitor;

public class ReportEvent implements Event<ReportEvent.Report> {

    private final Report report;

    public interface ReportType {}

    public enum ReportTypeAll implements ReportType {
        ALL(1), COMPETITOR_LIST(2), COMPETITOR_RANK(3), COMPETITOR_SUGGEST(4), MENU(5);

        private final int id;

        ReportTypeAll(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }

    public enum ReportTypeCompetitor implements  ReportType {
        COMPETITOR_HIST(100);

        private final int id;

        ReportTypeCompetitor(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }

    public static class Report {
        private final Competitor.CompetitorId competitorId;
        private final ReportType type;

        public Report(int competitorId, ReportType type) {
            this.competitorId = Competitor.CompetitorId.of(competitorId);
            this.type = type;
        }

        public Competitor.CompetitorId getCompetitorId() {
            return competitorId;
        }

        public ReportType getType() {
            return type;
        }
    }

    public ReportEvent(ReportTypeAll type) {
        report = new Report(-1, type);
    }

    public ReportEvent(int competitorId, ReportTypeCompetitor type) {
        report = new Report(competitorId, type);
    }

    @Override
    public EventType getEventType() {
        return EventType.REPORT_EVENT;
    }

    @Override
    public Report getEvent() {
        return report;
    }
}
