package org.kaerman.myelo.event;

public class SuggestEvent implements Event<Object>{
    @Override
    public EventType getEventType() {
        return EventType.SUGGEST_EVENT;
    }

    @Override
    public Object getEvent() {
        return null;
    }
}
