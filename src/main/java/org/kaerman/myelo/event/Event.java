package org.kaerman.myelo.event;

public interface Event<T> {
    enum EventType {MATCH_EVENT, ORDER_EVENT, REPORT_EVENT, SUGGEST_EVENT}

    EventType getEventType();

    T getEvent();
}
