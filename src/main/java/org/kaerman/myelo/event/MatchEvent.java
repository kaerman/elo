package org.kaerman.myelo.event;

import org.kaerman.myelo.domain.MatchResult;

public class MatchEvent implements Event<MatchResult> {
    private final MatchResult matchResult;

    public MatchEvent(Integer matchId, MatchResult matchResult) {
        this.matchResult = new MatchResult(matchId, matchResult.getWinnerCompetitorId(), matchResult.getLoserCompetitorId());
    }

    @Override
    public EventType getEventType() {
        return EventType.MATCH_EVENT;
    }

    @Override
    public MatchResult getEvent() {
        return matchResult;
    }
}
