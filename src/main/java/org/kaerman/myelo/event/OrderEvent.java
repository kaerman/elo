package org.kaerman.myelo.event;

public class OrderEvent implements Event<Object>{
    @Override
    public EventType getEventType() {
        return EventType.ORDER_EVENT;
    }

    @Override
    public Object getEvent() {
        return null;
    }
}
