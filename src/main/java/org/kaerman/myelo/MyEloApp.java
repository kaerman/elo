package org.kaerman.myelo;

import org.kaerman.myelo.consumer.ConsumerRegistry;
import org.kaerman.myelo.domain.CompetitorBoard;
import org.kaerman.myelo.domain.MatchResult;
import org.kaerman.myelo.event.*;
import org.kaerman.myelo.parser.CompetitorParser;
import org.kaerman.myelo.parser.MatchResultParser;
import org.kaerman.myelo.util.ConsoleUtil;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

import static org.kaerman.myelo.event.ReportEvent.*;

/**
 * My Elo App
 *
 */
public class MyEloApp
{
    public static final int NUMBER_OF_REQUIRED_ARGS = 2;
    private final CompetitorBoard competitorBoard;
    private final ConsumerRegistry consumerRegistry;

    private MyEloApp() {
        competitorBoard = CompetitorBoard.getInstance();
        consumerRegistry = ConsumerRegistry.getInstance();
    }

    private Map<Integer, MatchResult> loadAllFiles(String idNamePath, String matchesPath) throws IOException {
        MatchResultParser matchResultParser = new MatchResultParser();
        CompetitorParser competitorParser = new CompetitorParser();
        competitorBoard.loadCompetitors(competitorParser.parseAll(Paths.get(idNamePath)));
        return matchResultParser.parseAll(Paths.get(matchesPath));
    }

    private void processAllMatches(Map<Integer, MatchResult> allMatchResults) {
        allMatchResults.forEach((matchId, matchResult) -> consumerRegistry.fireEvent(new MatchEvent(matchId, matchResult)));
    }

    private void fireEvent(Event<?> event) {
        consumerRegistry.fireEvent(event);
    }

    private void calculateOrder() {
        fireEvent(new OrderEvent());
    }

    private void matchSuggest() {
        fireEvent(new SuggestEvent());
    }

    private void printResultStats() {
        fireEvent(new ReportEvent(ReportTypeAll.ALL));
    }

    public static void main( String[] args ) throws IOException {
        if (args.length != NUMBER_OF_REQUIRED_ARGS) {
            throw new IllegalArgumentException("Usage: java -jar MyElo.jar idNameFile.txt idScore.txt");
        }
        MyEloApp myEloApp = new MyEloApp();
        String idNameFilePath = args[0];
        String idScoreFilePath = args[1];
        Map<Integer, MatchResult> allMatchResults = myEloApp.loadAllFiles(idNameFilePath, idScoreFilePath);
        myEloApp.processAllMatches(allMatchResults);
        myEloApp.calculateOrder();
        myEloApp.matchSuggest();
        myEloApp.printResultStats();
        while (true) {
            ConsoleUtil.printMenu();
            myEloApp.fireEvent(ConsoleUtil.readCommand());
        }

    }

}
