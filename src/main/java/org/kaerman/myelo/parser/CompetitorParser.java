package org.kaerman.myelo.parser;

import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.exception.IllegalFormatException;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.kaerman.myelo.domain.Competitor.*;

public class CompetitorParser implements Parser<Competitor> {

    @Override
    public Map<CompetitorId, Competitor> parseAll(Path path) throws IOException {
        try (Stream<String> lines = Files.lines(path)) {
            return lines.filter(Parser::isNotEmptyLine)
                    .map(this::parse)
                    .collect(Collectors.toMap(Competitor::getId, competitor -> competitor));
        }
    }

    @Override
    public Competitor parse(String line) throws IllegalFormatException {
        StringTokenizer stringTokenizer = new StringTokenizer(line, WHITE_SPACE);
        if (stringTokenizer.countTokens() != VALID_TOKEN_NUMBER)
            throw new IllegalFormatException("CompetitorParser -> invalid token number, expected " + VALID_TOKEN_NUMBER + " actual " + stringTokenizer.countTokens());
        int id = parseCompetitorId(stringTokenizer.nextToken());
        String name = stringTokenizer.nextToken();
        return new Competitor(id, name);
    }
}
