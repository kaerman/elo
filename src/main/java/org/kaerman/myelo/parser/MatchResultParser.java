package org.kaerman.myelo.parser;

import org.kaerman.myelo.domain.MatchResult;
import org.kaerman.myelo.exception.IllegalFormatException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MatchResultParser implements Parser<MatchResult> {

    @Override
    public Map<Integer, MatchResult> parseAll(Path path) throws IOException {
        try (Stream<String> lines = Files.lines(path)) {
            AtomicInteger matchNumber = new AtomicInteger(0);
            return lines.filter(Parser::isNotEmptyLine)
                    .map(this::parse)
                    .collect(Collectors.toMap(matchResult -> matchNumber.incrementAndGet(), matchResult -> matchResult));
        }
    }

    @Override
    public MatchResult parse(String line) throws IllegalFormatException {
        StringTokenizer stringTokenizer = new StringTokenizer(line, WHITE_SPACE);
        if (stringTokenizer.countTokens() != VALID_TOKEN_NUMBER)
            throw new IllegalFormatException("MatchResultParser -> invalid token number, expected " + VALID_TOKEN_NUMBER + " actual " + stringTokenizer.countTokens());
        int winnerId = parseCompetitorId(stringTokenizer.nextToken());
        int loserId = parseCompetitorId(stringTokenizer.nextToken());
        return new MatchResult(winnerId, loserId);
    }
}
