package org.kaerman.myelo.parser;

import org.kaerman.myelo.exception.IllegalFormatException;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

public interface Parser<T> {

    int MIN_COMPETITOR_ID = 0;
    int VALID_TOKEN_NUMBER = 2;
    String WHITE_SPACE = " ";

    Map<?, T> parseAll(Path path) throws IOException;

    T parse(String line) throws IllegalFormatException;

    static boolean isNotEmptyLine(String line) {
        return line.length() != 0 && !line.isBlank();
    }

   default Integer parseCompetitorId(String idStr) throws IllegalFormatException {
        int id;
        try {
            id = Integer.parseInt(idStr);
            if (id >= 0) {
                return id;
            }
        } catch (NumberFormatException ignored) {
        }
        throw new IllegalFormatException(this.getClass() + " -> invalid competitor id=" + idStr);
    }
}
