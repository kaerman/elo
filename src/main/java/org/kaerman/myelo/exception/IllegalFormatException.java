package org.kaerman.myelo.exception;

public class IllegalFormatException extends IllegalArgumentException {
    public IllegalFormatException(String message) {
        super(message);
    }
}
