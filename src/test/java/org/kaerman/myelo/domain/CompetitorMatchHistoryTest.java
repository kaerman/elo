package org.kaerman.myelo.domain;

import org.junit.jupiter.api.Test;
import org.kaerman.myelo.event.MatchEvent;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CompetitorMatchHistoryTest {

    @Test
    void addMatch() {
        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
        Map<Competitor.CompetitorId, Competitor> competitorMap =  new HashMap<>();
        Competitor competitor1 = new Competitor(1, "Kamil");
        competitor1.setRank(2300);
        competitorMap.put(Competitor.CompetitorId.of(1), competitor1);
        competitorBoard.loadCompetitors(competitorMap);

        MatchEvent matchEvent = new MatchEvent(10, new MatchResult(1, 2));
        competitorBoard.addMatchResultMap(matchEvent.getEvent());
        assertEquals(competitorBoard.getMatchResultMap().get(matchEvent.getEvent().getMatchId()), matchEvent.getEvent());

        CompetitorBoard.getInstance().getCompetitorHistory(competitor1.getId()).addMatch(matchEvent.getEvent().getMatchId());
    }
}