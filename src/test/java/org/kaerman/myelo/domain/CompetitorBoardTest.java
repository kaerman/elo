package org.kaerman.myelo.domain;

import org.junit.jupiter.api.Test;
import org.kaerman.myelo.event.MatchEvent;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CompetitorBoardTest {

    @Test
    void loadCompetitors() {
        Map<Competitor.CompetitorId, Competitor> competitorMap =  new HashMap<>();
        Competitor competitor1 = new Competitor(1, "Kamil");
        competitor1.setRank(2300);
        Competitor competitor2 = new Competitor(2, "Erman");
        competitor2.setRank(2500);
        competitorMap.put(Competitor.CompetitorId.of(1), competitor1);
        competitorMap.put(Competitor.CompetitorId.of(2), competitor2);
        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
        competitorBoard.loadCompetitors(competitorMap);

        assertEquals(competitorBoard.getCompetitorMap().get(competitor1.getId()), competitor1);
        assertEquals(competitorBoard.getCompetitorMap().get(competitor2.getId()), competitor2);
    }

    @Test
    void addMatchResultMap() {
        MatchEvent matchEvent = new MatchEvent(10, new MatchResult(1, 2));

        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
        competitorBoard.addMatchResultMap(matchEvent.getEvent());

        assertEquals(competitorBoard.getMatchResultMap().get(matchEvent.getEvent().getMatchId()), matchEvent.getEvent());

    }
}