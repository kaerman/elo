package org.kaerman.myelo.domain;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.kaerman.myelo.domain.Competitor.*;

class CompetitorAvailableOpponentsTest {

    @Test
    void removeOpponent() {
        Competitor competitor1 = new Competitor(1, "Kamil");
        competitor1.setRank(2300);
        Competitor competitor2 = new Competitor(2, "Erman");
        competitor2.setRank(2500);
        Set<CompetitorId> competitorAvailableOpponentSet = new HashSet<>();
        competitorAvailableOpponentSet.add(competitor1.getId());
        competitorAvailableOpponentSet.add(competitor2.getId());

        CompetitorAvailableOpponents competitorAvailableOpponents = new CompetitorAvailableOpponents(competitorAvailableOpponentSet);
        competitorAvailableOpponents.removeOpponent(competitor1.getId());

        assertFalse(competitorAvailableOpponents.getOpponentCompetitorSet().contains(competitor1.getId()));
        assertTrue(competitorAvailableOpponents.getOpponentCompetitorSet().contains(competitor2.getId()));
    }

    @Test
    void getOpponentCompetitorSet() {
    }
}