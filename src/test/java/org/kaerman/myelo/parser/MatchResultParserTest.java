package org.kaerman.myelo.parser;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.kaerman.myelo.domain.MatchResult;
import org.kaerman.myelo.exception.IllegalFormatException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.kaerman.myelo.parser.Parser.MIN_COMPETITOR_ID;

/**
 * Unit test CompetitorParser
 */
public class MatchResultParserTest
{
    public static final int TOTAL_MATCH_NUMBER = 100;
    public static final String CANNOT_ACCESS_TEST_FILE = "Cannot access test file";
    private static Path idScorePath;
    private static String successLine;
    private static String invalidTokenNumberLine;
    private static String invalidCompetitorIdLine;
    private static MatchResultParser matchResultParser;

    private static String getFirstLine(Path path) throws IOException {
        try (Stream<String> lines = Files.lines(path)) {
            return lines.filter(Parser::isNotEmptyLine)
                    .findFirst()
                    .orElseThrow(() -> new NoSuchElementException("No valid line"));
        }
    }

    @BeforeAll
    public static void init() {
        matchResultParser = new MatchResultParser();
        idScorePath = Paths.get("src", "test", "resources", "competitor_parser", "idScore.txt");
        Path idScoreInvalidTokenNumberPath = Paths.get("src", "test", "resources", "competitor_parser", "idScoreInvalidTokenNumber.txt");
        Path idScoreInvalidCompetitorIdPath = Paths.get("src", "test", "resources", "competitor_parser", "idScoreInvalidCompetitorId.txt");

        assertTrue(idScorePath.toFile().isFile(), CANNOT_ACCESS_TEST_FILE);
        assertTrue(idScoreInvalidTokenNumberPath.toFile().isFile(), CANNOT_ACCESS_TEST_FILE);
        assertTrue(idScoreInvalidCompetitorIdPath.toFile().isFile(), CANNOT_ACCESS_TEST_FILE);

        try {
            successLine = getFirstLine(idScorePath);
            invalidTokenNumberLine = getFirstLine(idScoreInvalidTokenNumberPath);
            invalidCompetitorIdLine = getFirstLine(idScoreInvalidCompetitorIdPath);
        } catch (IOException ioe) {
            fail(ioe.getMessage());
        }

    }

    @Test
    public void parseCompetitorLine_throwsIllegalFormatException_WithMessage_InvalidTokenNumber() {
        String expectedMessage = "MatchResultParser -> invalid token number, expected";
        Exception exception = assertThrows(IllegalFormatException.class,
                () -> matchResultParser.parse(invalidTokenNumberLine));
        assertTrue(exception.getMessage().contains(expectedMessage), "invalidTokenNumberLine -> " + invalidTokenNumberLine);
    }

    @Test
    public void parseCompetitorLine_throwsIllegalFormatException_WithMessage_InvalidCompetitorId() {
        String expectedMessage = "MatchResultParser -> invalid competitor id";
        Exception exception = assertThrows(IllegalFormatException.class,
                () -> matchResultParser.parse(invalidCompetitorIdLine));
        assertTrue(exception.getMessage().contains(expectedMessage), "invalidCompetitorIdLine -> " + invalidCompetitorIdLine);
    }

    @Test
    public void parseCompetitorLine_SuccessCase() {
        MatchResult matchResult = matchResultParser.parse(successLine);
        assertTrue(matchResult.getWinnerCompetitorId().getValue() >= MIN_COMPETITOR_ID);
        assertTrue(matchResult.getLoserCompetitorId().getValue() >= MIN_COMPETITOR_ID);
    }

    @Test
    public void readAllIdNameFile_SuccessCase() {
        try {
            Map<Integer, MatchResult> competitorMap = matchResultParser.parseAll(idScorePath);
            assertEquals(competitorMap.size(), TOTAL_MATCH_NUMBER);
        } catch (IOException ioException) {
            fail(ioException.getMessage());
        }
    }
}
