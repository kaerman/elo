package org.kaerman.myelo.parser;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.exception.IllegalFormatException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.kaerman.myelo.parser.Parser.MIN_COMPETITOR_ID;

/**
 * Unit test CompetitorParser
 */
public class CompetitorParserTest
{
    public static final int COMPETITOR_NUMBER = 40;
    public static final String CANNOT_ACCESS_TEST_FILE = "Cannot access test file";
    private static Path idNamePath;
    private static String successLine;
    private static String invalidTokenNumberLine;
    private static String invalidCompetitorIdLine;
    private static CompetitorParser competitorParser;

    private static String getFirstLine(Path path) throws IOException {
        try (Stream<String> lines = Files.lines(path)) {
            return lines.filter(Parser::isNotEmptyLine)
                    .findFirst()
                    .orElseThrow(() -> new NoSuchElementException("No valid line"));
        }
    }

    @BeforeAll
    public static void init() {
        competitorParser = new CompetitorParser();
        idNamePath = Paths.get("src", "test", "resources", "competitor_parser", "idName.txt");
        Path idNameInvalidTokenNumberPath = Paths.get("src", "test", "resources", "competitor_parser", "idNameInvalidTokenNumber.txt");
        Path idNameInvalidCompetitorIdPath = Paths.get("src", "test", "resources", "competitor_parser", "idNameInvalidCompetitorId.txt");

        assertTrue(idNamePath.toFile().isFile(), CANNOT_ACCESS_TEST_FILE);
        assertTrue(idNameInvalidTokenNumberPath.toFile().isFile(), CANNOT_ACCESS_TEST_FILE);
        assertTrue(idNameInvalidCompetitorIdPath.toFile().isFile(), CANNOT_ACCESS_TEST_FILE);

        try {
            successLine = getFirstLine(idNamePath);
            invalidTokenNumberLine = getFirstLine(idNameInvalidTokenNumberPath);
            invalidCompetitorIdLine = getFirstLine(idNameInvalidCompetitorIdPath);
        } catch (IOException ioe) {
            fail(ioe.getMessage());
        }

    }

    @Test
    public void parseCompetitorLine_throwsIllegalFormatException_WithMessage_InvalidTokenNumber() {
        String expectedMessage = "CompetitorParser -> invalid token number, expected";
        Exception exception = assertThrows(IllegalFormatException.class,
                () -> competitorParser.parse(invalidTokenNumberLine));
        assertTrue(exception.getMessage().contains(expectedMessage), "invalidTokenNumberLine -> " + invalidTokenNumberLine);
    }

    @Test
    public void parseCompetitorLine_throwsIllegalFormatException_WithMessage_InvalidCompetitorId() {
        String expectedMessage = "CompetitorParser -> invalid competitor id";
        Exception exception = assertThrows(IllegalFormatException.class,
                () -> competitorParser.parse(invalidCompetitorIdLine));
        assertTrue(exception.getMessage().contains(expectedMessage), "invalidCompetitorIdLine -> " + invalidCompetitorIdLine);
    }

    @Test
    public void parseCompetitorLine_SuccessCase() {
        Competitor competitor = competitorParser.parse(successLine);
        assertTrue(competitor.getId().getValue() >= MIN_COMPETITOR_ID);
        assertTrue(competitor.getName().length() > 0);
    }

    @Test
    public void readAllIdNameFile_SuccessCase() {
        try {
            Map<Competitor.CompetitorId, Competitor> competitorMap = competitorParser.parseAll(idNamePath);
            assertEquals(competitorMap.size(), COMPETITOR_NUMBER);
        } catch (IOException ioException) {
            fail(ioException.getMessage());
        }
    }
}
