package org.kaerman.myelo.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EloUtilTest {

    @Test
    void calculateKFactor() {
        assertEquals(EloUtil.calculateKFactor(220), 32);
        assertEquals(EloUtil.calculateKFactor(2200), 24);
        assertEquals(EloUtil.calculateKFactor(22000), 16);
    }

    @Test
    void calculateWinProbability() {
        double winProbability = EloUtil.calculateWinProbability(1000, 1000);
        assertEquals(winProbability, 0.5);
        winProbability = EloUtil.calculateWinProbability(2000, 1000);
        assertTrue(winProbability > 0.5);
        winProbability = EloUtil.calculateWinProbability(1000, 2000);
        assertTrue(winProbability < 0.5);
    }

    @Test
    void calculateEloRating() {
        double newScore = EloUtil.calculateEloRating(2200, 1, true);
        assertEquals(newScore, 2200);
        newScore = EloUtil.calculateEloRating(2200, 1, false);
        assertTrue(newScore < 2200);
        newScore = EloUtil.calculateEloRating(2200, 0.5, true);
        assertTrue(newScore > 2200);
        newScore = EloUtil.calculateEloRating(2200, 0.5, false);
        assertTrue(newScore < 2200);
    }
}