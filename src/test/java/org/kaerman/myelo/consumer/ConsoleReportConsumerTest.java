package org.kaerman.myelo.consumer;

import org.junit.jupiter.api.Test;
import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.domain.CompetitorBoard;
import org.kaerman.myelo.event.ReportEvent;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.kaerman.myelo.event.Event.EventType.REPORT_EVENT;
import static org.kaerman.myelo.event.ReportEvent.ReportTypeAll.*;

class ConsoleReportConsumerTest {

    @Test
    void registerEvents() {
        ConsoleReportConsumer consoleReportConsumer = new ConsoleReportConsumer();
        assertTrue(consoleReportConsumer.registerEvents().contains(REPORT_EVENT));
    }

    @Test
    void consume() {
        ConsoleReportConsumer consoleReportConsumer = new ConsoleReportConsumer();
        ReportEvent reportEvent = new ReportEvent(COMPETITOR_LIST);
        consoleReportConsumer.consume(reportEvent);

        Map<Competitor.CompetitorId, Competitor> competitorMap =  new HashMap<>();
        Competitor competitor1 = new Competitor(1, "Kamil");
        competitor1.setRank(2300);
        Competitor competitor2 = new Competitor(2, "Erman");
        competitor2.setRank(2500);
        competitorMap.put(Competitor.CompetitorId.of(1), competitor1);
        competitorMap.put(Competitor.CompetitorId.of(2), competitor2);
        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
        competitorBoard.loadCompetitors(competitorMap);

        consoleReportConsumer.consume(reportEvent);
    }
}