package org.kaerman.myelo.consumer;

import org.junit.jupiter.api.Test;
import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.domain.CompetitorBoard;
import org.kaerman.myelo.domain.MatchResult;
import org.kaerman.myelo.event.MatchEvent;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.kaerman.myelo.event.Event.EventType.MATCH_EVENT;

class EloRankingConsumerTest {

    @Test
    void registerEvents() {
        EloRankingConsumer eloRankingConsumer = new EloRankingConsumer();
        assertTrue(eloRankingConsumer.registerEvents().contains(MATCH_EVENT));
    }

    @Test
    void consume() {
        EloRankingConsumer eloRankingConsumer = new EloRankingConsumer();
        MatchEvent matchEvent = new MatchEvent(0, new MatchResult(1, 2));

        Map<Competitor.CompetitorId, Competitor> competitorMap =  new HashMap<>();
        Competitor competitor1 = new Competitor(1, "Kamil");
        competitor1.setRank(2300);
        Competitor competitor2 = new Competitor(2, "Erman");
        competitor2.setRank(2500);
        competitorMap.put(Competitor.CompetitorId.of(1), competitor1);
        competitorMap.put(Competitor.CompetitorId.of(2), competitor2);
        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
        competitorBoard.loadCompetitors(competitorMap);

        eloRankingConsumer.consume(matchEvent);
        assertTrue(CompetitorBoard.getInstance().getCompetitorMap().get(competitor1.getId()).getRank() > 2300);
        assertTrue(CompetitorBoard.getInstance().getCompetitorMap().get(competitor2.getId()).getRank() < 2500);
    }
}