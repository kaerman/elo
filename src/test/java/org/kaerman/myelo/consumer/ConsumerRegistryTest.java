package org.kaerman.myelo.consumer;

import org.junit.jupiter.api.Test;
import org.kaerman.myelo.domain.MatchResult;
import org.kaerman.myelo.event.Event;
import org.kaerman.myelo.event.MatchEvent;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.kaerman.myelo.event.Event.*;

class ConsumerRegistryTest {

    private static class TestConsumer implements Consumer {

        private Event<MatchResult> receivedEvent;

        @Override
        public List<EventType> registerEvents() {
            return Collections.singletonList(EventType.MATCH_EVENT);
        }

        @Override
        public void consume(Event<?> event) {
            if (event instanceof MatchEvent) {
                receivedEvent = (MatchEvent) event;
            }
        }
    }

    @Test
    void addConsumer() {
        ConsumerRegistry consumerRegistry = ConsumerRegistry.getInstance();
        TestConsumer testConsumer = new TestConsumer();
        consumerRegistry.addConsumer(testConsumer);
    }

    @Test
    void fireEvent() {
        ConsumerRegistry consumerRegistry = ConsumerRegistry.getInstance();
        TestConsumer testConsumer = new TestConsumer();
        consumerRegistry.addConsumer(testConsumer);

        MatchEvent matchEvent = new MatchEvent(0, new MatchResult(1, 2));
        consumerRegistry.fireEvent(matchEvent);

        assertEquals(testConsumer.receivedEvent, matchEvent);
    }
}