package org.kaerman.myelo.consumer;

import org.junit.jupiter.api.Test;
import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.domain.CompetitorBoard;
import org.kaerman.myelo.event.SuggestEvent;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.kaerman.myelo.event.Event.EventType.SUGGEST_EVENT;

class MatchSuggestConsumerTest {

    @Test
    void registerEvents() {
        MatchSuggestConsumer matchSuggestConsumer = new MatchSuggestConsumer();
        assertTrue(matchSuggestConsumer.registerEvents().contains(SUGGEST_EVENT));
    }

    @Test
    void consume() {
        MatchSuggestConsumer matchSuggestConsumer = new MatchSuggestConsumer();
        SuggestEvent suggestEvent = new SuggestEvent();

        Map<Competitor.CompetitorId, Competitor> competitorMap =  new HashMap<>();
        Competitor competitor1 = new Competitor(1, "Kamil");
        competitor1.setRank(2300);
        Competitor competitor2 = new Competitor(2, "Erman");
        competitor2.setRank(2500);
        competitorMap.put(Competitor.CompetitorId.of(1), competitor1);
        competitorMap.put(Competitor.CompetitorId.of(2), competitor2);
        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
        competitorBoard.loadCompetitors(competitorMap);

        matchSuggestConsumer.consume(suggestEvent);

        CompetitorBoard.getInstance().getSuggestedMatches();
        assertEquals(CompetitorBoard.getInstance().getSuggestedMatches().size(), 1);
    }
}