package org.kaerman.myelo.consumer;

import org.junit.jupiter.api.Test;
import org.kaerman.myelo.domain.Competitor;
import org.kaerman.myelo.domain.CompetitorBoard;
import org.kaerman.myelo.event.OrderEvent;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.kaerman.myelo.event.Event.EventType.*;

class CompetitorOrderConsumerTest {

    @Test
    void registerEvents() {
        CompetitorOrderConsumer competitorOrderConsumer = new CompetitorOrderConsumer();
        assertTrue(competitorOrderConsumer.registerEvents().contains(ORDER_EVENT));
    }

    @Test
    void consume() {
        CompetitorOrderConsumer competitorOrderConsumer = new CompetitorOrderConsumer();
        OrderEvent orderEvent = new OrderEvent();
        competitorOrderConsumer.consume(orderEvent);

        Map<Competitor.CompetitorId, Competitor> competitorMap =  new HashMap<>();
        Competitor competitor1 = new Competitor(1, "Kamil");
        competitor1.setRank(2300);
        Competitor competitor2 = new Competitor(2, "Erman");
        competitor2.setRank(2500);
        competitorMap.put(Competitor.CompetitorId.of(1), competitor1);
        competitorMap.put(Competitor.CompetitorId.of(2), competitor2);
        CompetitorBoard competitorBoard = CompetitorBoard.getInstance();
        competitorBoard.loadCompetitors(competitorMap);

        competitorOrderConsumer.consume(orderEvent);
        assertEquals(competitorBoard.getCompetitorRankList().get(0), competitor2);
    }
}